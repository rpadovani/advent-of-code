def calculate(right, down, elements):
    total = 0
    for i, row in enumerate(elements):
        if i % down != 0:
            continue

        if row[(right * int(((i - 1) / down + 1))) % len(row)] == '#':
            total += 1

    return total


def resolver():
    with open('input.txt', 'r') as reader:
        elements = reader.read().splitlines()

    totals = [
        calculate(1, 1, elements),
        calculate(3, 1, elements),
        calculate(5, 1, elements),
        calculate(7, 1, elements),
        calculate(1, 2, elements),
    ]

    print(totals)

    gran_total = 1
    for total in totals:
        gran_total *= total

    print(gran_total)


if __name__ == '__main__':
    resolver()
