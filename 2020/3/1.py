def resolver():
    with open('input.txt', 'r') as reader:
        elements = reader.read().splitlines()

    total = 0
    for i, row in enumerate(elements):
        if row[(3 * i) % len(row)] == '#':
            total += 1

    print(total)


if __name__ == '__main__':
    resolver()
