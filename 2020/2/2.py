def resolver():
    total = 0
    with open('input.txt', 'r') as reader:
        while True:
            line = reader.readline()

            if not line:
                break

            first_occurrence = int(line.split(' ')[0].split('-')[0])
            second_occurrence = int(line.split(' ')[0].split('-')[1])
            letter = line.split(' ')[1].split(':')[0]
            word = line.split(' ')[2].rstrip()

            if (word[first_occurrence - 1] == letter) ^ (word[second_occurrence - 1] == letter):
                total += 1

    print(total)


if __name__ == '__main__':
    resolver()
