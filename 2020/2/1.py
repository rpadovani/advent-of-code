def resolver():
    total = 0
    with open('input.txt', 'r') as reader:
        while True:
            line = reader.readline()

            if not line:
                break

            min_occurrences = int(line.split(' ')[0].split('-')[0])
            max_occurrences = int(line.split(' ')[0].split('-')[1])
            letter = line.split(' ')[1].split(':')[0]
            word = line.split(' ')[2].rstrip()

            if min_occurrences <= word.count(letter) <= max_occurrences:
                total += 1

    print(total)


if __name__ == '__main__':
    resolver()
