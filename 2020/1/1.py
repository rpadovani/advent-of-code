def resolver():
    with open('input.txt', 'r') as reader:
        elements = reader.read().splitlines()

    elements = [int(i) for i in elements]
    elements.sort()
    highest_to_lowest = elements.copy()
    highest_to_lowest.reverse()

    for highest in highest_to_lowest:
        for lowest in elements:
            if highest + lowest == 2020:
                print(highest * lowest)
                exit(0)

            if highest + lowest > 2020:
                break

    exit(1)


if __name__ == '__main__':
    resolver()
