def resolver():
    with open('input.txt', 'r') as reader:
        elements = reader.read().splitlines()

    elements = [int(i) for i in elements]
    elements.sort()
    highest_to_lowest = elements.copy()
    highest_to_lowest.reverse()

    for highest in highest_to_lowest:
        for i, lowest in enumerate(elements):
            if lowest + elements[i + 1] + highest > 2020:
                break

            for middle_number in elements[i + 1:]:
                total = highest + lowest + middle_number

                if total == 2020:
                    print(f'Numbers: {highest:} {lowest:} {middle_number:}')
                    print(f'Result: {highest * lowest * middle_number}')
                    exit(0)

                if total > 2020:
                    break

    exit(1)


if __name__ == '__main__':
    resolver()
